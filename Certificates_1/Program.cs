﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Certificates_1
{
    class Program
    {
        static void Main(string[] args)
        {
            //I have already located makecert and used it to create a new certificate
            //this program only works if makercert has already been used and the certificate is already created.

            //on my PC, I found makecert at this location C:\Program Files (x86)\Windows Kits\10\bin\x64
            //and I used Windows PowerShell (running in admin mode as always) to create the certificate

            //if you dont have makecert in your PC, you probably have not installed the windows SDK in VS

            //this is the command I used 
            //in the beginning I have used the '.\' because powershell told me to like that
            //.\MakeCert -n "CN=studynildana" -sr currentuser -ss testCertStore

            //this command has now successfully created and installed a certificate into the certificate-store called testCertStore

            //now that my computer has a certificate (which is a combination fo a hash and asymmetric key) lets use it

            //first let me get the message to encrypt
            Console.WriteLine("Enter a sentence");

            string input_string_from_user = Console.ReadLine();

            //here I am going to get the signed message i.e. hashed and encrypted
            byte[] signed_string_byte = sign_the_input_string(input_string_from_user, "cn=studynildana");

            //now I have the signed string. let me check if the hashed string is identical to the hashed string that I get from the 
            //signed byte stream
            bool check_the_hash = compare_the_two(input_string_from_user, signed_string_byte);

            if(check_the_hash == true)
            {
                Console.WriteLine("Of course they are same!");
            }
            else
            {
                //check notes about this
                Console.WriteLine("They are not same how is this possible!");
            }


            //I will use the powers vested in me to block the console window from dissapearing
            Console.ReadLine();


        }

        //this method will check if the hash of the string, and the hash from the signed byte stream is the same
        //if it is false, that means either the signed byte array has been tampered
        //or, the string is tampered.
        //if neither is tampered with, the returned value will always be true
        private static bool compare_the_two(string input_string_from_user, byte[] signed_string_byte)
        {
            bool bool_to_return = false;

            //let me collect the certificate
            X509Certificate2 certificate_to_use = grab_certificate_from_store();

            //first hash the string provided.
            byte[] hash_of_input_string = hash_the_string(input_string_from_user);

            //now, I will compare this hash of the string with hash of the signed byte array
            //get a cryptography object
            //I used the private key to encrypt
            //So I will now use the public key to decrypt
            var cryptography_object = (RSACryptoServiceProvider)certificate_to_use.PublicKey.Key;

            bool_to_return = cryptography_object.VerifyHash(hash_of_input_string, CryptoConfig.MapNameToOID("SHA1"), signed_string_byte);

            return (bool_to_return);
        }

        //this is the method that will sign the text using the certificate with the name 'studynildana'
        private static byte[] sign_the_input_string(string input_string_from_user, string v)
        {
            //first let me grab the certificate to sign
            X509Certificate2 certificate_to_use = grab_certificate_from_store();

            //now I need to get the private key
            var cryptography_object = (RSACryptoServiceProvider)certificate_to_use.PrivateKey;

            //first I need to hash the message
            byte[] hashed_input_string = hash_the_string(input_string_from_user);

            //now, I will encrypt the hash string.
            //since I am hashing and encrypting, the signature is complete

            //I will use the crypto object above - which has the private key -
            //the first parameter is the hashed byte stream 
            //second is the type of encryption I wish to use. In this case SHA1
            return cryptography_object.SignHash(hashed_input_string, CryptoConfig.MapNameToOID("SHA1"));
        }

        //this will has the message and return the byte stream
        private static byte[] hash_the_string(string input_string_from_user)
        {
            //first convert the string into byte stream
            UnicodeEncoding encoding_object = new UnicodeEncoding();
            byte[] byte_converted_input_string_from_user = encoding_object.GetBytes(input_string_from_user);

            //now lest hash this byte stream
            //get the hashing object
            HashAlgorithm hashing_object = new SHA1Managed();
            byte[] hashed_byte_stream = hashing_object.ComputeHash(byte_converted_input_string_from_user);

            //lets return the hashed byte stream
            return hashed_byte_stream;
            
        }

        //this is the method that will grab the certificate from the store
        private static X509Certificate2 grab_certificate_from_store()
        {
            //the first paramater is the store name. this was given when I installed the certificate 
            //second parameter is the username where certificate store is located
            X509Store certificate_store = new X509Store("testCertStore", StoreLocation.CurrentUser);

            //the mode in which I plan to look into the certificate store
            certificate_store.Open(OpenFlags.ReadOnly);

            //now pulling out the certificate I want
            //I am using the 0th item becuase I want the very first (and the only ) certificate in the store
            X509Certificate2 certificate_to_return = certificate_store.Certificates[0];

            //returning the obtained certificate
            return (certificate_to_return);
        }

        //this method will get me the certificate from the certificate store
    }
}
